﻿using Category.MicroService.Models;
using Category.MicroService.Query;
using Category.MicroService.Services;
using MediatR;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Category.MicroService.Handler
{
    public class GetCategoriesHandler : IRequestHandler<GetCategoriesQuery, IEnumerable<EcomCategory>>
    {
        private readonly ICategoryService _categoryService;

        public GetCategoriesHandler(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        public async Task<IEnumerable<EcomCategory>> Handle(GetCategoriesQuery request, CancellationToken cancellationToken)
        {
            return await _categoryService.GetAll();
        }
    }
}
