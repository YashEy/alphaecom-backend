﻿using Category.MicroService.Models;
using Category.MicroService.Query;
using Category.MicroService.Services;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Category.MicroService.Handler
{
    public class GetCategoryByIdHandler : IRequestHandler<GetCategoryByIdQuery, EcomCategory>
    {
        private readonly ICategoryService _categoryService;

        public GetCategoryByIdHandler(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        public async Task<EcomCategory> Handle(GetCategoryByIdQuery request, CancellationToken cancellationToken)
        {
            return await _categoryService.GetById(request.CategoryId);
        }
    }
}
