﻿using Category.MicroService.Models;
using MediatR;
using System.Collections;
using System.Collections.Generic;

namespace Category.MicroService.Command
{
    public class AddCategoryCommand : IRequest<IEnumerable<EcomCategory>>
    {
        public EcomCategory Category { get; set; }
    }
}
