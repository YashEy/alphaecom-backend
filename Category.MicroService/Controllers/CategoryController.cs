﻿using Category.MicroService.Command;
using Category.MicroService.Logger;
using Category.MicroService.Models;
using Category.MicroService.Query;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Category.MicroService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ISender _sender;
        private ILoggerService _loggerService;

        public CategoryController(ISender sender, ILoggerService loggerService)
        {
            _sender = sender;
            _loggerService = loggerService;
        }
        [HttpGet]
        public async Task<IEnumerable<EcomCategory>> GetAll()
        {
            _loggerService.LogInfo("Fetching All Categories");
            return await _sender.Send(new GetCategoriesQuery());
        }

        [HttpGet("{id}")]
        public async Task<EcomCategory> GetById(int id)
        {
            return await _sender.Send(new GetCategoryByIdQuery() { CategoryId = id });
        }

        [HttpPost("add")]
        public async Task<IEnumerable<EcomCategory>> AddCategory([FromBody] EcomCategory category)
        {
            return await _sender.Send(new AddCategoryCommand() { Category = category });
        }


    }
}
