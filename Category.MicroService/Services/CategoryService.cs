﻿using Category.MicroService.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Category.MicroService.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly EComContext _eComContext;

        public CategoryService(EComContext eComContext)
        {
            _eComContext = eComContext;
        }

        public async Task<IEnumerable<EcomCategory>> AddCategory(EcomCategory category)
        {
            _eComContext.EcomCategory.Add(category);
            await _eComContext.SaveChangesAsync();
            return await GetAll();
        }

        public async Task<IEnumerable<EcomCategory>> GetAll()
        {
            return await Task.FromResult(_eComContext.EcomCategory.ToList());
        }

        public async Task<EcomCategory> GetById(int id)
        {
            return await Task.FromResult(_eComContext.EcomCategory.SingleOrDefault(x => x.CategoryId == id));
        }
    }
}
