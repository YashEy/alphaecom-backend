﻿using Category.MicroService.Models;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Category.MicroService.Services
{
    public interface ICategoryService
    {
        Task<IEnumerable<EcomCategory>> AddCategory(EcomCategory category);
        Task<EcomCategory> GetById(int id);
        Task<IEnumerable<EcomCategory>> GetAll();
    }
}
