﻿using Category.MicroService.Models;
using MediatR;
using System.Collections;
using System.Collections.Generic;

namespace Category.MicroService.Query
{
    public class GetCategoriesQuery : IRequest<IEnumerable<EcomCategory>>
    {
    }
}
