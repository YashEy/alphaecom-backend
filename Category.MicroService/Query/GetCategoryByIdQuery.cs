﻿using Category.MicroService.Models;
using MediatR;

namespace Category.MicroService.Query
{
    public class GetCategoryByIdQuery:IRequest<EcomCategory>
    {
        public int CategoryId { get; set; }
    }
}
