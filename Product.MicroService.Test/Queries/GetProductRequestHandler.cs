﻿using Moq;
using Product.Microservice.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Xunit;
using Product.Microservice.Handler;
using Product.Microservice.Query;
using Product.Microservice.Models;
using Shouldly;
using System.Linq;

namespace Product.MicroService.Test.Queries
{
    public class GetProductRequestHandler
    {
        private readonly Mock<IProductService> _mockRepo;
        public GetProductRequestHandler()
        {
            //_mockRepo = Mock.MockRepository.GetProductService();
        }

        [Fact]
        public async Task GetCategoryListTest()
        {
            var handler = new GetProductsHandler(_mockRepo.Object);

            var result = await handler.Handle(new GetProductQuery(), CancellationToken.None);

            result.ShouldBeOfType<List<EcomProducts>>();

            result.Count().ShouldBe(1);
        }
    }
}
