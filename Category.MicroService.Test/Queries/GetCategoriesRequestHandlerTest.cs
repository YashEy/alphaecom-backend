﻿using Category.MicroService.Handler;
using Category.MicroService.Query;
using Category.MicroService.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Xunit;
using Category.MicroService.Models;
using Shouldly;
using System.Linq;

namespace Category.MicroService.Test.Queries
{
    public class GetCategoriesRequestHandlerTest
    {
        private readonly Mock<ICategoryService> _mockRepo;

        public GetCategoriesRequestHandlerTest()
        {
            _mockRepo = Mocks.MockRepository.GetCategoryService();
        }

        [Fact]
        public async Task GetCategoryListTest()
        {
            var handler = new GetCategoriesHandler(_mockRepo.Object);

            var result = await handler.Handle(new GetCategoriesQuery(), CancellationToken.None);

            result.ShouldBeOfType<List<EcomCategory>>();

            result.Count().ShouldBe(2);
        }

    }
}
