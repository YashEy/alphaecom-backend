﻿using Category.MicroService.Command;
using Category.MicroService.Handler;
using Category.MicroService.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Xunit;
using Category.MicroService.Models;
using Shouldly;
using System.Linq;

namespace Category.MicroService.Test.Commands
{
    public class AddCategoryHandlerTest
    {
        private readonly Mock<ICategoryService> _mockRepo;
        private readonly EcomCategory _category;

        public AddCategoryHandlerTest()
        {
            _mockRepo = Mocks.MockRepository.GetCategoryService();

            _category = new EcomCategory
            {
                CategoryId = 506,
                CategoryName = "Stationary"
            };
        }

        [Fact]
        public async Task AddCategoryTest()
        {
            var handler = new AddCategoryHandler(_mockRepo.Object);

            var result = handler.Handle(new AddCategoryCommand { Category = _category }, CancellationToken.None);

            var category = await _mockRepo.Object.GetAll();

            await result.ShouldBeOfType<Task<IEnumerator<EcomCategory>>>();

            category.Count().ShouldBe(3);
        }

    }
}
