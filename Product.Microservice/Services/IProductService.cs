﻿using Product.Microservice.Models;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Product.Microservice.Services
{
    public interface IProductService
    {
        Task<IEnumerable<EcomProducts>> AddProduct(EcomProducts product);
        Task<IEnumerable<EcomProducts>> UpdateProduct(EcomProducts product);
        Task<IEnumerable<EcomProducts>> GetAllProduducts();
        Task<EcomProducts> DeleteProductbyId(int id);
        Task<EcomProducts> GetById(int id);
        Task<IEnumerable<EcomProducts>> GetProductByCategoryId(int categoryId);

    }
}
