﻿using Product.Microservice.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Product.Microservice.Services
{
    public class ProductService : IProductService
    {
        private readonly EComContext _eComContext;

        public ProductService(EComContext db)
        {
            _eComContext = db;
        }

        public async Task<IEnumerable<EcomProducts>> AddProduct(EcomProducts product)
        {
           _eComContext.EcomProducts.Add(product);
            await _eComContext.SaveChangesAsync();
            return await GetAllProduducts();
        }

        public async Task<EcomProducts> DeleteProductbyId(int id)
        {
           var del= await GetById(id);
            _eComContext.EcomProducts.Remove(del);
           return del;

        }

        public async Task<IEnumerable<EcomProducts>> GetAllProduducts()
        {
            return await Task.FromResult(_eComContext.EcomProducts.ToList());
        }

        public async Task<EcomProducts> GetById(int id)
        {
            return await Task.FromResult(_eComContext.EcomProducts.SingleOrDefault(o => o.ProductId == id));
        }

        public async Task<IEnumerable<EcomProducts>> GetProductByCategoryId(int categoryId)
        {
            return await Task.FromResult(_eComContext.EcomProducts.Where(x => x.CategoryId == categoryId));
        }

        public async Task<IEnumerable<EcomProducts>> UpdateProduct(EcomProducts product)
        {
            await DeleteProductbyId(product.ProductId);
            _eComContext.EcomProducts.Add(product);
            await _eComContext.SaveChangesAsync();
            return await GetAllProduducts();

        }
    }

        
}
