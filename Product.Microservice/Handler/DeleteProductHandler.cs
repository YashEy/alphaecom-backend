﻿using MediatR;
using Product.Microservice.Command;
using Product.Microservice.Models;
using Product.Microservice.Services;
using System.Threading;
using System.Threading.Tasks;

namespace Product.Microservice.Handler
{
    public class DeleteProductHandler : IRequestHandler<DeleteProductCommand, EcomProducts>
    {
        private readonly IProductService _productService;

        public DeleteProductHandler(IProductService productService)
        {
            _productService = productService;
        }

        public async Task<EcomProducts> Handle(DeleteProductCommand request, CancellationToken cancellationToken)
        {
            return await _productService.DeleteProductbyId(request.ProductId);
        }
    }
}
