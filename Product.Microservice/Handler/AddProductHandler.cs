﻿using MediatR;
using Product.Microservice.Command;
using Product.Microservice.Models;
using Product.Microservice.Services;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Product.Microservice.Handler
{
    public class AddProductHandler : IRequestHandler<AddProductCommand, IEnumerable<EcomProducts>>
    {
        private readonly IProductService _productService;

        public AddProductHandler(IProductService productService)
        {
            _productService = productService;
        }

        public async Task<IEnumerable<EcomProducts>> Handle(AddProductCommand request, CancellationToken cancellationToken)
        {
            return await _productService.AddProduct(request.Product);
        }
    }
}
