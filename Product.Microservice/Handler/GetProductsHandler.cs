﻿using MediatR;
using Product.Microservice.Models;
using Product.Microservice.Query;
using Product.Microservice.Services;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Product.Microservice.Handler
{
    public class GetProductsHandler : IRequestHandler<GetProductQuery, IEnumerable<EcomProducts>>
    {
        private readonly IProductService _productService;

        public GetProductsHandler(IProductService productService)
        {
            _productService = productService;
        }

        public async Task<IEnumerable<EcomProducts>> Handle(GetProductQuery request, CancellationToken cancellationToken)
        {
            return await _productService.GetAllProduducts();
        }
    }
}
