﻿using MediatR;
using Product.Microservice.Query;
using Product.Microservice.Services;
using System.Threading.Tasks;
using System.Threading;
using Product.Microservice.Models;

namespace Product.Microservice.Handler
{
    public class GetProductByIdHandler : IRequestHandler<GetProductByIdQuery, EcomProducts>
    {
        private readonly IProductService _productService;

        public GetProductByIdHandler(IProductService productService)
        {
            _productService = productService;
        }

        public async Task<EcomProducts> Handle(GetProductByIdQuery request, CancellationToken cancellationToken)
        {
            return await _productService.GetById(request.ProductId);
        }
    }

    
}
