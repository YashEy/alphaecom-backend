﻿using MediatR;
using Product.Microservice.Models;
using Product.Microservice.Query;
using Product.Microservice.Services;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Product.Microservice.Handler
{
    public class GetProductByCategoryIdHandler : IRequestHandler<GetProductByCategoryIdQuery, IEnumerable<EcomProducts>>
    {
        private readonly IProductService service;

        public GetProductByCategoryIdHandler(IProductService productService)
        {
            service = productService;
        }

        public async Task<IEnumerable<EcomProducts>> Handle(GetProductByCategoryIdQuery request, CancellationToken cancellationToken)
        {
            return await service.GetProductByCategoryId(request.CategoryId);
        }
    }
}
