﻿using MediatR;
using Product.Microservice.Command;
using Product.Microservice.Models;
using Product.Microservice.Services;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Product.Microservice.Handler
{
    public class UpdateProductHandler : IRequestHandler<UpdateProductCommand, IEnumerable<EcomProducts>>
    {
        private readonly IProductService _productService;

        public UpdateProductHandler(IProductService productService)
        {
            _productService = productService;
        }

        public async Task<IEnumerable<EcomProducts>> Handle(UpdateProductCommand request, CancellationToken cancellationToken)
        {
            return await _productService.UpdateProduct(request.Product);
        }
    }
}
