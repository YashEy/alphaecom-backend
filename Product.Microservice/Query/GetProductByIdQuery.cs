﻿using MediatR;
using Product.Microservice.Models;

namespace Product.Microservice.Query
{
    public class GetProductByIdQuery : IRequest<EcomProducts>
    {
        public int ProductId { get; set; }
    }

}
