﻿using MediatR;
using Product.Microservice.Models;
using System.Collections.Generic;

namespace Product.Microservice.Query
{
    public class GetProductByCategoryIdQuery : IRequest<IEnumerable<EcomProducts>>
    {
        public int CategoryId { get; set; }
    }

}
