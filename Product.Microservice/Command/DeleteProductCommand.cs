﻿using MediatR;
using Product.Microservice.Models;

namespace Product.Microservice.Command
{
    public class DeleteProductCommand : IRequest<EcomProducts>
    {
        public int ProductId { get; set; }
    }

}
