﻿using MediatR;
using Product.Microservice.Models;
using System.Collections.Generic;

namespace Product.Microservice.Command
{
    public class UpdateProductCommand : IRequest<IEnumerable<EcomProducts>>
    {
        public EcomProducts Product { get; set; }
    }

}
