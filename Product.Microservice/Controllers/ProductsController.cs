﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Product.Microservice.Command;
using Product.Microservice.Models;
using Product.Microservice.Query;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Product.Microservice.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly ISender _sender;

        public ProductsController(ISender sender)
        {
            _sender = sender;
        }

        [HttpGet]
        public async Task<IEnumerable<EcomProducts>> GetAllProducts()
        {
            return await _sender.Send(new GetProductQuery());
        }

        [HttpGet("getbyid/{id}")]
        public async Task<EcomProducts> GetById(int id)
        {
            return await _sender.Send(new GetProductByIdQuery() { ProductId = id });
        }

        [HttpPost("add")]
        public async Task<IEnumerable<EcomProducts>> AddProduct([FromBody] EcomProducts product)
        {
            return await _sender.Send(new AddProductCommand() { Product = product });
        }

        [HttpDelete("deletebyid/{id}")]
        public async Task<EcomProducts> DeleteProductById(int id)
        {
            return await _sender.Send(new DeleteProductCommand() { ProductId = id });
        }

        [HttpPut("update")]
        public async Task<IEnumerable<EcomProducts>> UpdateProduct([FromBody] EcomProducts product)
        {
            return await _sender.Send(new UpdateProductCommand() { Product = product });
        }

        [HttpGet("category/{categoryId}")]
        public async Task<IEnumerable<EcomProducts>> GetProsuctsByCategoryId(int categoryId)
        {
            return await _sender.Send(new GetProductByCategoryIdQuery() { CategoryId = categoryId });
        }

    }
}
