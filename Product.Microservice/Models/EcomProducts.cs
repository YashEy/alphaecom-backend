﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Product.Microservice.Models
{
    public partial class EcomProducts
    {
        public EcomProducts()
        {
            EcomOrders = new HashSet<EcomOrders>();
        }

        public int ProductId { get; set; }
        public int CategoryId { get; set; }
        public string ProductName { get; set; }
        public string ProductType { get; set; }
        public decimal ProductPrice { get; set; }
        public string ProductDescription { get; set; }
        public string ProductImageUrl { get; set; }

        public virtual EcomProduct Category { get; set; }
        public virtual ICollection<EcomOrders> EcomOrders { get; set; }
    }
}
