﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Product.Microservice.Models
{
    public partial class EcomLogin
    {
        public EcomLogin()
        {
            EcomCustomers = new HashSet<EcomCustomers>();
        }

        public int LoginId { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
        public DateTime DateTimeStamp { get; set; }
        public string LoginRole { get; set; }
        public string UserId { get; set; }

        public virtual ICollection<EcomCustomers> EcomCustomers { get; set; }
    }
}
