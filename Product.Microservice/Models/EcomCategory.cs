﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Product.Microservice.Models
{
    public partial class EcomProduct
    {
        public EcomProduct()
        {
            EcomProducts = new HashSet<EcomProducts>();
        }

        public int CategoryId { get; set; }
        public string CategoryName { get; set; }

        public virtual ICollection<EcomProducts> EcomProducts { get; set; }
    }
}
