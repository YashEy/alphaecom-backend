﻿using MediatR;
using Payment.Microservice.Models;

namespace Payment.Microservice.Query
{
    public class GetPaymentByIdQuery : IRequest<EcomPayment>
    {
        public int Id { get; set; }
    }
}

