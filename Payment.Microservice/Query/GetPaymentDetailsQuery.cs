﻿using MediatR;
using Payment.Microservice.Models;
using System.Collections.Generic;

namespace Payment.Microservice.Query
{
    public class GetPaymentDetailsQuery : IRequest<IEnumerable<EcomPayment>>
    {
    }

}
