﻿using MediatR;
using Payment.Microservice.Command;
using Payment.Microservice.Models;
using Payment.Microservice.Services;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Payment.Microservice.Handler
{
    public class AddPaymentHandler : IRequestHandler<AddPaymentCommand, IEnumerable<EcomPayment>>
    {
        private readonly IPaymentService _paymentService;

        public AddPaymentHandler(IPaymentService paymentService)
        {
            _paymentService = paymentService;
        }

        public async Task<IEnumerable<EcomPayment>> Handle(AddPaymentCommand request, CancellationToken cancellationToken)
        {
            return await _paymentService.AddPayment(request.Payment);
        }
    }
}
