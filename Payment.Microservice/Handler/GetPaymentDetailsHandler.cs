﻿using MediatR;
using Payment.Microservice.Models;
using Payment.Microservice.Query;
using Payment.Microservice.Services;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Payment.Microservice.Handler
{
    public class GetPaymentDetailsHandler : IRequestHandler<GetPaymentDetailsQuery, IEnumerable<EcomPayment>>
    {
        private readonly IPaymentService _paymentService;

        public GetPaymentDetailsHandler(IPaymentService paymentService)
        {
            _paymentService = paymentService;
        }

        public async Task<IEnumerable<EcomPayment>> Handle(GetPaymentDetailsQuery request, CancellationToken cancellationToken)
        {
            return await _paymentService.GetAll();
        }
    }
}
