﻿using MediatR;
using Payment.Microservice.Models;
using Payment.Microservice.Query;
using Payment.Microservice.Services;
using System.Threading;
using System.Threading.Tasks;

namespace Payment.Microservice.Handler
{
    public class GetPaymentByIdHandler : IRequestHandler<GetPaymentByIdQuery, EcomPayment>
    {
        private readonly IPaymentService _paymentService;

        public GetPaymentByIdHandler(IPaymentService paymentService)
        {
            _paymentService = paymentService;
        }

        public async Task<EcomPayment> Handle(GetPaymentByIdQuery request, CancellationToken cancellationToken)
        {
            return await _paymentService.GetById(request.Id);
        }
    }
}
