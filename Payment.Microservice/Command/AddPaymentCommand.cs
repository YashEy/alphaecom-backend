﻿using MediatR;
using Payment.Microservice.Models;
using System.Collections.Generic;

namespace Payment.Microservice.Command
{
    public class AddPaymentCommand : IRequest<IEnumerable<EcomPayment>>
    {
        public EcomPayment Payment { get; set; }
    }

}
