﻿using Customer.Microservice.Models;
using MediatR;
using System.Collections.Generic;

namespace Customer.Microservice.Command
{
    public class AddCustomerCommand : IRequest<EcomCustomers>
    {
        public EcomCustomers Customer { get; set; }
    }
}
