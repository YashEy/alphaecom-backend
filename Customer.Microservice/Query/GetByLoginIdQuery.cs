﻿using Customer.Microservice.Models;
using MediatR;

namespace Customer.Microservice.Query
{
    public class GetByLoginIdQuery : IRequest<EcomCustomers>
    {
        public int Id { get; set; }
    }

}
