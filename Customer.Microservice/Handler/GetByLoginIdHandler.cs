﻿using Customer.Microservice.Models;
using Customer.Microservice.Query;
using Customer.Microservice.Services;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Customer.Microservice.Handler
{
    public class GetByLoginIdHandler : IRequestHandler<GetByLoginIdQuery, EcomCustomers>
    {
        private readonly ICustomerService _customerService;

        public GetByLoginIdHandler(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        public async Task<EcomCustomers> Handle(GetByLoginIdQuery request, CancellationToken cancellationToken)
        {
            return await _customerService.GetByLoginId(request.Id);
        }
    }
}
