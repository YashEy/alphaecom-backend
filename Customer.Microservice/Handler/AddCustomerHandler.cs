﻿using Customer.Microservice.Command;
using Customer.Microservice.Models;
using Customer.Microservice.Services;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Customer.Microservice.Handler
{
    public class AddCustomerHandler : IRequestHandler<AddCustomerCommand, EcomCustomers>
    {
        private readonly ICustomerService _customerService;

        public AddCustomerHandler(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        public async Task<EcomCustomers> Handle(AddCustomerCommand request, CancellationToken cancellationToken)
        {
            return await _customerService.AddCustomer(request.Customer);
        }

    }
}
