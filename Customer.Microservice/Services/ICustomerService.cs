﻿using Customer.Microservice.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Customer.Microservice.Services
{
    public interface ICustomerService
    {
        Task<EcomCustomers> AddCustomer(EcomCustomers customer);
        Task<EcomCustomers> GetByLoginId(int id);

    }
}
