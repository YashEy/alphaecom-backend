﻿using Customer.Microservice.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Customer.Microservice.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly EComContext _db;

        public CustomerService(EComContext db)
        {
            _db = db;
        }

        public async Task<EcomCustomers> AddCustomer(EcomCustomers customer)
        {
            _db.EcomCustomers.Add(customer);
            await _db.SaveChangesAsync();
            return _db.EcomCustomers.SingleOrDefault(x => x.LoginId == customer.LoginId);

        }

        public async Task<EcomCustomers> GetByLoginId(int id)
        {
            return await Task.FromResult(_db.EcomCustomers.SingleOrDefault(x => x.LoginId == id));
        }
    }
}
