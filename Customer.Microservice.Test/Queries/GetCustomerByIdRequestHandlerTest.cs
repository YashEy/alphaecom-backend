﻿using Customer.Microservice.Handler;
using Customer.Microservice.Query;
using Customer.Microservice.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Xunit;
using Shouldly;
using Customer.Microservice.Models;

namespace Customer.Microservice.Test.Queries
{
    public class GetCustomerByIdRequestHandlerTest
    {
        private readonly Mock<ICustomerService> _mockRepo;

        public GetCustomerByIdRequestHandlerTest()
        {
            _mockRepo = Mocks.MockRepository.GetCustomerService();
        }

        [Fact]
        public async Task GetCustomerByIdTest()
        {
            var handler = new GetByLoginIdHandler(_mockRepo.Object);

            var result = handler.Handle(new GetByLoginIdQuery(), CancellationToken.None);

            await result.ShouldBeOfType<Task<EcomCustomers>>();
        }

    }
}
