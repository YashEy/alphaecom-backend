﻿using Customer.Microservice.Models;
using Customer.Microservice.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Customer.Microservice.Test.Mocks
{
    public class MockRepository
    {
        public static Mock<ICustomerService> GetCustomerService()
        {
            var cuslist = new List<EcomCustomers>
            {
                new EcomCustomers
                {
                    CustomerId = 1,
                    CustomerName = "Sharan",
                    CustomerAddress = "Banglore",
                    CustomerPhoneNumber = "1234567890",
                    CustomerEmailId = "sharan@gmail.com",
                    LoginId = 101
                },
                new EcomCustomers
                {
                    CustomerId = 2,
                    CustomerName = "Harsh",
                    CustomerAddress = "Banglore",
                    CustomerPhoneNumber = "1234567890",
                    CustomerEmailId = "harsh@gmail.com",
                    LoginId = 102
                }
            };

            var mockrepo = new Mock<ICustomerService>();

            mockrepo.Setup(r => r.GetByLoginId(101)).ReturnsAsync(cuslist.SingleOrDefault(x => x.LoginId == 101));

            //mockrepo.Setup(r => r.AddCustomer(It.IsAny<EcomCustomers>())).ReturnsAsync((EcomCustomers customer) =>
            //{
            //    cuslist.Add(customer);
            //    return cuslist;
            //});

            return mockrepo;
        }

    }
}
