﻿using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Xunit;
using Login.Microservice.Services;
using Login.Microservice.Models;
using Login.Microservice.Handler;
using Login.Microservice.Command;

namespace Login.MicroService.Test.Commands
{
    public class AddLoginCommandHandlerTest
    {
        private readonly Mock<ILoginService> _mockRepo;
        private readonly EcomLogin _login;

        public AddLoginCommandHandlerTest()
        {
            _mockRepo = Mocks.MockRepository.GetLoginService();

            _login = new EcomLogin
            {
                LoginId = 100,
                Password = "Thejas@25win",
                Token = "123456789qwertyui",
                LoginRole = "USER",
                UserId = "Thejaswin"
            };
        }

        [Fact]
        public async Task AddLoginTest()
        {
            var handler = new AddLoginHandler(_mockRepo.Object);

            var result = handler.Handle(new AddLoginCommand(), CancellationToken.None);
        }

    }
}

