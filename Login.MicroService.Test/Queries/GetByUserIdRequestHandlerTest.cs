﻿using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Xunit;
using Login.Microservice.Services;
using Login.Microservice.Handler;
using Login.Microservice.Query;
using Shouldly;
using Login.Microservice.Models;

namespace Login.MicroService.Test.Queries
{
    public class GetByUserIdRequestHandlerTest
    {
        private readonly Mock<ILoginService> _mockRepo;

        public GetByUserIdRequestHandlerTest()
        {
            _mockRepo = Mocks.MockRepository.GetLoginService();
        }

        [Fact]
        public async Task GetByUderIdTest()
        {
            var handler = new GetByIdHandler(_mockRepo.Object);

            var result = handler.Handle(new GetByIdQuery(), CancellationToken.None);

            await result.ShouldBeOfType<Task<EcomLogin>>();
        }
    }
}
