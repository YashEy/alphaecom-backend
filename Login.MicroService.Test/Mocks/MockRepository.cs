﻿using Login.Microservice.Models;
using Login.Microservice.Services;
using Moq;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Login.MicroService.Test.Mocks
{
    internal class MockRepository
    {
        public static Mock<ILoginService> GetLoginService()
        {
            var catlist = new List<EcomLogin>
            {
                new EcomLogin
                {
                    //CategoryId = 300,
                    //CategoryName = "Dress"
                    LoginId=100,
                    Password="Thejas@25win",
                    Token="123456789qwertyui",
                    LoginRole ="USER",
                    UserId= "Thejaswin"
                }
            };

            var mockrepo = new Mock<ILoginService>();

            //mockrepo.Setup(r => r.GetByUserId()).ReturnsAsync(catlist);
            mockrepo.Setup(r => r.GetByUserId("Thejaswin")).ReturnsAsync(catlist.SingleOrDefault(x => x.UserId == "Thejaswin"));

            //mockrepo.Setup(r => r.AddLogin(It.IsAny<EcomLogin>())).ReturnsAsync((EcomLogin login) =>
            //{
            //    catlist.Add(login);
            //    return catlist;
            //});

            return mockrepo;
        }
    }
}
