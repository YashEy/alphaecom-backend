﻿using MediatR;
using Orders.Microservice.Models;
using System.Collections.Generic;

namespace Orders.Microservice.Query
{
    public class GetOrdersbyIdQuery : IRequest<IEnumerable<EcomOrders>>
    {
        public int CustomerId { get; set; }

    }
}
