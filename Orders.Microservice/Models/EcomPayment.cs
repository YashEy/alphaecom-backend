﻿using System;
using System.Collections.Generic;

namespace Orders.Microservice.Models
{
    public partial class EcomPayment
    {
        public int PaymentId { get; set; }
        public int OrderId { get; set; }
        public string PaymentMode { get; set; }
        public string CardNumber { get; set; }
        public int? CardCvv { get; set; }
        public DateTime? CardExpiry { get; set; }
        public string CardName { get; set; }

        public virtual EcomOrders Order { get; set; }
    }
}
