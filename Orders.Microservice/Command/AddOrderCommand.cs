﻿using MediatR;
using Orders.Microservice.Models;
using System.Collections.Generic;

namespace Orders.Microservice.Command
{
    public class AddOrderCommand : IRequest<EcomOrders>
    {
        public EcomOrders Order { get; set; }

    }
}
