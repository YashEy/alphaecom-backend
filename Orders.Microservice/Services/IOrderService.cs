﻿using Orders.Microservice.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Orders.Microservice.Services
{
    public interface IOrderService
    {

        Task<EcomOrders> AddOrder(EcomOrders order);
        Task<IEnumerable<EcomOrders>> GetAllById(int custid);

    }
}
