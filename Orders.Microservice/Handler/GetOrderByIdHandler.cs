﻿using MediatR;
using Orders.Microservice.Handler;
using Orders.Microservice.Query;
using Orders.Microservice.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using Orders.Microservice.Models;

namespace Orders.Microservice.Handler
{
    public class GetOrderByIdHandler : IRequestHandler<GetOrdersbyIdQuery, IEnumerable<EcomOrders>>
    {
        private readonly IOrderService _orderService;

        public GetOrderByIdHandler(IOrderService orderService)
        {
            _orderService = orderService;
        }
        public Task<IEnumerable<EcomOrders>> Handle(GetOrdersbyIdQuery request, CancellationToken cancellationToken)
        {
            return _orderService.GetAllById(request.CustomerId);
        }
    }
}
