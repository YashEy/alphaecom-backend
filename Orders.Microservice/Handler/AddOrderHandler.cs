﻿using MediatR;
using Orders.Microservice.Command;
using Orders.Microservice.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using Orders.Microservice.Models;

namespace Orders.Microservice.Handler
{
    public class AddOrderHandler : IRequestHandler<AddOrderCommand, EcomOrders>
    {
        private readonly IOrderService _orderService;

        public AddOrderHandler(IOrderService orderService)
        {
            _orderService = orderService;
        }

        public  Task<EcomOrders> Handle(AddOrderCommand request, CancellationToken cancellationToken)
        {
            return  _orderService.AddOrder(request.Order);
        }
    }

}
