﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Orders.Microservice.Command;
using Orders.Microservice.Models;
using Orders.Microservice.Query;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Orders.Microservice.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly ISender _sender;

        public OrdersController(ISender sender)
        {
            _sender = sender;
        }
        [HttpGet("{custid}")]
        public async Task<IEnumerable<EcomOrders>> GetAll(int custid)
        {
            return await _sender.Send(new GetOrdersbyIdQuery() { CustomerId = custid });
        }

        [HttpPost("add")]
        public async Task<EcomOrders> AddOrder([FromBody] EcomOrders order)
        {
            return await _sender.Send(new AddOrderCommand() { Order = order });
        }

    }
}
