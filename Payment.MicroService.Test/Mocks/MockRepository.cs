﻿using Moq;
using Payment.Microservice.Models;
using Payment.Microservice.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Payment.MicroService.Test.Mocks
{
    public class MockRepository
    {
        public static Mock<IPaymentService> GetPaymentService()
        {
            var cuslist = new List<EcomPayment>
            {
                new EcomPayment
                {
                    //CustomerId = 1,
                    //CustomerName = "Sharan",
                    //CustomerAddress = "Banglore",
                    //CustomerPhoneNumber = "1234567890",
                    //CustomerEmailId = "sharan@gmail.com",
                    //LoginId = 101
                    PaymentId=1,
                    OrderId=700,
                    PaymentMode="CARD",
                    CardNumber="12345678987654",
                    CardCvv=123,
                    CardName="Thejaswin"

                }
            };

            var mockrepo = new Mock<IPaymentService>();

            mockrepo.Setup(r => r.GetAll()).ReturnsAsync(cuslist);

            mockrepo.Setup(r => r.AddPayment(It.IsAny<EcomPayment>())).ReturnsAsync((EcomPayment payment) =>
            {
                cuslist.Add(payment);
                return cuslist;
            });

            return mockrepo;

            
        }
    }
}
