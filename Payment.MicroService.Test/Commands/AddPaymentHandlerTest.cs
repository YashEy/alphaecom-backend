﻿using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Xunit;
using Payment.Microservice.Services;
using Payment.Microservice.Models;
using Payment.Microservice.Handler;
using Payment.Microservice.Command;
using Shouldly;
using System.Linq;

namespace Payment.MicroService.Test.Commands
{
    public class AddPaymentHandlerTest
    {
        private readonly Mock<IPaymentService> _mockRepo;
        private readonly EcomPayment _category;

        public AddPaymentHandlerTest()
        {
            _mockRepo = Mocks.MockRepository.GetPaymentService();

            _category = new EcomPayment
            {
                PaymentId = 1,
                OrderId = 700,
                PaymentMode = "CARD",
                CardNumber = "12345678987654",
                CardCvv = 123,
                CardName = "Thejaswin"
            };
        }

        [Fact]
        public async Task AddPaymentTest()
        {
            var handler = new AddPaymentHandler(_mockRepo.Object);

            var result = handler.Handle(new AddPaymentCommand { Payment = _category }, CancellationToken.None);

            var category = await _mockRepo.Object.GetAll();

            await result.ShouldBeOfType<Task<IEnumerator<EcomPayment>>>();

            category.Count().ShouldBe(3);
        }
    }
}
