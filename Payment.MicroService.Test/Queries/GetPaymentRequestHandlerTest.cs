﻿using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Xunit;
using Payment.Microservice.Services;
using Payment.Microservice.Handler;
using Payment.Microservice.Query;
using Shouldly;
using Payment.Microservice.Models;

namespace Payment.MicroService.Test.Queries
{
    public class GetPaymentRequestHandlerTest
    {
        private readonly Mock<IPaymentService> _mockRepo;

        public GetPaymentRequestHandlerTest()
        {
            _mockRepo = Mocks.MockRepository.GetPaymentService();
        }

        [Fact]
        public async Task GetPaymentListTest()
        {
            var handler = new GetPaymentByIdHandler(_mockRepo.Object);

            var result = await handler.Handle(new GetPaymentByIdQuery(), CancellationToken.None);

            result.ShouldBeOfType<List<EcomPayment>>();

            //result.Count().ShouldBe(2);
        }
    }
}
