﻿using Login.Microservice.Models;
using Login.Microservice.Query;
using Login.Microservice.Services;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Login.Microservice.Handler
{
    public class GetByIdHandler : IRequestHandler<GetByIdQuery, EcomLogin>
    {
        private readonly ILoginService _loginService;

        public GetByIdHandler(ILoginService loginService)
        {
            _loginService = loginService;
        }

        public async Task<EcomLogin> Handle(GetByIdQuery request, CancellationToken cancellationToken)
        {
            return await _loginService.GetByUserId(request.UserId);
        }
    }
}
