﻿using Login.Microservice.Command;
using Login.Microservice.Models;
using Login.Microservice.Services;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Login.Microservice.Handler
{
    public class AddLoginHandler : IRequestHandler<AddLoginCommand, EcomLogin>
    {
        private readonly ILoginService _loginService;

        public AddLoginHandler(ILoginService loginService)
        {
            _loginService = loginService;
        }

        public async Task<EcomLogin> Handle(AddLoginCommand request, CancellationToken cancellationToken)
        {
            return await _loginService.AddLogin(request.Login);
        }
    }
}
