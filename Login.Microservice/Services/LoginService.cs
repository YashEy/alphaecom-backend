﻿using Login.Microservice.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Login.Microservice.Services
{
    public class LoginService : ILoginService
    {
        private readonly EComContext _db;
        private readonly IConfiguration _config;
        public LoginService(EComContext db, IConfiguration config)
        {
            _db = db;
            _config = config;
        }

        private string GenerateToken(EcomLogin user)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier,user.UserId),
                new Claim(ClaimTypes.Role,user.LoginRole)
            };
            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
                _config["Jwt:Audience"],
                claims,
                expires: DateTime.Now.AddMinutes(15),
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }


        public async Task<EcomLogin> AddLogin(EcomLogin login)
        {
            login.Token = GenerateToken(login);
            login.DateTimeStamp = DateTime.Now;
            _db.EcomLogin.Add(login);
            await _db.SaveChangesAsync();
            return await Task.FromResult(_db.EcomLogin.SingleOrDefault(x => x.UserId == login.UserId));

        }


        public async Task<EcomLogin> GetByUserId(string userid)
        {
            return await Task.FromResult(_db.EcomLogin.SingleOrDefault(x => x.UserId == userid));
        }
    }
}
