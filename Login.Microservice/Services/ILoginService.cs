﻿using Login.Microservice.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Login.Microservice.Services
{
    public interface ILoginService
    {
        Task<EcomLogin> AddLogin(EcomLogin login);
        Task<EcomLogin> GetByUserId(string userid);

    }
}
