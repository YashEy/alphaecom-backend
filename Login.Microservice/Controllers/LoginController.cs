﻿using Login.Microservice.Command;
using Login.Microservice.Models;
using Login.Microservice.Query;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Login.Microservice.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private ISender _sender;

        public LoginController(ISender sender)
        {
            _sender = sender;
        }

        [HttpGet("{userid}")]
        public async Task<EcomLogin> GetById(string userid)
        {
            return await _sender.Send(new GetByIdQuery() { UserId = userid });
        }


        [HttpPost("add")]
        public async Task<EcomLogin> AddLogin([FromBody] EcomLogin login)
        {
            return await _sender.Send(new AddLoginCommand() { Login = login });
        }


    }
}
