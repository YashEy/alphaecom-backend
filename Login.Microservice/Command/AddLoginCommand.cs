﻿using Login.Microservice.Models;
using MediatR;
using System.Collections.Generic;

namespace Login.Microservice.Command
{
    public class AddLoginCommand : IRequest<EcomLogin>
    {
        public EcomLogin Login { get; set; }
    }
}
