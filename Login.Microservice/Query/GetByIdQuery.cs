﻿using Login.Microservice.Models;
using MediatR;

namespace Login.Microservice.Query
{
    public class GetByIdQuery : IRequest<EcomLogin>
    {
        public string UserId { get; set; }
    }

}
