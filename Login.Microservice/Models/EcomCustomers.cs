﻿using System;
using System.Collections.Generic;

namespace Login.Microservice.Models
{
    public partial class EcomCustomers
    {
        public EcomCustomers()
        {
            EcomOrders = new HashSet<EcomOrders>();
        }

        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerPhoneNumber { get; set; }
        public string CustomerEmailId { get; set; }
        public int LoginId { get; set; }

        public virtual EcomLogin Login { get; set; }
        public virtual ICollection<EcomOrders> EcomOrders { get; set; }
    }
}
