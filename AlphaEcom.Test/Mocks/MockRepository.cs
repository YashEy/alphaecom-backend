﻿using Castle.Core.Resource;
using Moq;
using Orders.Microservice.Models;
using Orders.Microservice.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orders.MicroService.Test.Mocks
{
    public class MockRepository
    {
        public static Mock<IOrderService> GetCategoryService()
        {
            var catlist = new List<EcomOrders>
            {
                new EcomOrders
                {
                    //CategoryId = 300,
                    //CategoryName = "Dress"
                    OrderId=03,
                    ProductId=505,
                    CustomerId=101,
                    OrderQuantity=1,
                    OrderPrice=1000,
                    ShipmentAddress="matam"

                },
                new EcomOrders
                {
                    OrderId=03,
                    ProductId=505,
                    CustomerId=101,
                    OrderQuantity=1,
                    OrderPrice=1000,
                    ShipmentAddress="matam"
                }
            };

            var mockrepo = new Mock<IOrderService>();

            //mockrepo.Setup(r => r.GetAllById()).ReturnsAsync(catlist);

            //mockrepo.Setup(r => r.AddOrder(It.IsAny<EcomOrders>())).ReturnsAsync((EcomCategory category) =>
            //{
            //    catlist.Add(Orders);
            //    return catlist;
            //});

            return mockrepo;
        }
    }
}
